import { Greetings } from './greetings';

export const greetings = [
  new Greetings(1, 'Hello'),
  new Greetings(2, 'Hi'),
  new Greetings(3, 'Welcome'),
  new Greetings(4, 'HO! HO! HO!'),
];
