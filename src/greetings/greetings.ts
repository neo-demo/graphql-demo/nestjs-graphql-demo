import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Greetings {
  @Field(() => ID)
  id: number;

  @Field(() => String)
  greetings: string;

  constructor(id?: number, greetings?: string) {
    this.id = id;
    this.greetings = greetings;
  }
}
