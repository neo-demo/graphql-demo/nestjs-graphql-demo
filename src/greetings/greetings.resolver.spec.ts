import { Test, TestingModule } from '@nestjs/testing';
import { GreetingsResolver } from './greetings.resolver';

describe('GreetingsResolver', () => {
  let resolver: GreetingsResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GreetingsResolver],
    }).compile();

    resolver = module.get<GreetingsResolver>(GreetingsResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
