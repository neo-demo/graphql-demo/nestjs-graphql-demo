import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { Greetings } from './greetings';
import { greetings } from './greetings.data';

@Resolver(() => Greetings)
export class GreetingsResolver {
  @Query(() => Greetings)
  getGreetings(@Args('id', { nullable: false }) id: number) {
    return greetings.find((value) => value.id == id);
  }
  @Query(() => [Greetings])
  getAllGreetings() {
    return greetings;
  }
  @Mutation(() => Greetings)
  createGreetings(@Args('greeting') greeting: string) {
    greetings.push(new Greetings(greetings.length + 1, greeting));
    return greetings[greetings.length - 1];
  }
  @Mutation(() => String)
  removeGreetings(@Args('id', { nullable: false }) id: number) {
    greetings.forEach((item, index) => {
      if (item.id === id) greetings.splice(index, 1);
    });
    return 'Greetings removed: ' + id;
  }
}
