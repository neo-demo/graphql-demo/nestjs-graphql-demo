import { Module } from '@nestjs/common';
import { GreetingsResolver } from './greetings.resolver';

@Module({
  providers: [GreetingsResolver],
})
export class GreetingsModule {}
